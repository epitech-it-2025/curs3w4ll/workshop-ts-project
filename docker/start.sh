#!/bin/bash

set -o pipefail

buildedFile=/tmp/builded.txt

function build() {( set -e
    echo "Start building api..."

    cd /src
    mkdir /app
    export NODE_ENV="production"
    npm run build
    cp build /app/ -r
    cp node_modules /app/ -r
    rm -rf /src

    echo "api successfully builded"
    echo "yes" > $buildedFile
)}

if [ ! -f $buildedFile ]; then
    build
    if [ $? -ne 0 ]; then
        echo "ERROR: Fail to build api !"
        exit 1
    fi
fi

cd /app
node build/server.js

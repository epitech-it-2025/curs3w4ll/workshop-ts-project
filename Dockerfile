FROM node:19-alpine

ARG API_VERSION
ENV API_VERSION ${API_VERSION}
ENV PATH /src/node_modules/.bin:$PATH

WORKDIR /src

COPY . ./
COPY ./docker/start.sh /start.sh

RUN apk add bash
RUN npm install

RUN chmod +x /start.sh

EXPOSE 5000

CMD ["bash", "/start.sh"]

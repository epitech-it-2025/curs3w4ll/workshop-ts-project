import express, { Express, Request, Response, NextFunction } from "express";
import cors from "cors";
import helmet from "helmet";
import { StatusCodes } from "http-status-codes";

const app: Express = express();

async function start() {
  app.use(helmet({
    crossOriginResourcePolicy: false,
  }));
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());
  app.use(cors());

  app.get("/", (_req: Request, res: Response) => {
    res.status(StatusCodes.OK).json({ message: "Hello World !" });
  });

  app.use(function(_req: Request, res: Response) {
    res.status(StatusCodes.NOT_FOUND).send();
  });

  app.listen(5555, () => {
    console.log("The server is running on port 5555");
  });
}

start();
